# Python Date와 Time: Datetime 사용법

## <a name="intro"></a> 개요
이 포스팅에서는 Python에서 `datetime` 모듈을 사용하여 날짜와 시간을 다루는 방법을 배울 것이다. 날짜와 시간은 스케줄링, 로깅, 데이터 분석 등 다양한 작업에서 사용될 수 있기 때문에 모든 프로그래밍 언어에서 필수적인 개념이다. 그러나 날짜와 시간을 사용하는 것도 쉽지 않을 수 있다. 이는 다양한 형식, 시간대, 일광 절약 시간 등을 고려해야 하기 때문이다.

Python의 datetime 모듈은 날짜와 시간 객체를 간단하고 일관된 방식으로 조작할 수 있도록 도와주는 일련의 클래스와 함수를 제공한다. 다음과 같은 방법을 배울 것이다.

- 다양한 입력에서 datetime 객체 생성
- 산술 연산 및 메서드를 사용하여 datetime 객체 조작
- 문자열을 사용하여 datetime 개체 형식 지정과 구문 분석
- `pytz`` 모듈을 사용하여 시간대 및 일광 절약 시간 처리
- `timedelta`와 `dateutil` 모듈을 사용하여 날짜와 시간에 따라 보다 복잡한 계산 수행

이 포그팅의 내용을 알고나면 Python에서 날짜와 시간을 사용하는 방법과 날짜 시간 모듈을 효과적으로 사용하는 방법에 대해 잘 이해할 수 있을 것이다.

시작합시다!

## <a name="sec_02"></a> `datetime` 모듈이란?
`datetime` 모듈은 Python에 내장된 모듈로 날짜와 시간 객체와 함께 작업할 수 있는 일련의 클래스와 함수를 제공한다. datetime 모듈은 표준 라이브러리의 일부이므로 이를 사용하기 위해 아무것도 설치할 필요가 없다. `import` 문을 사용하여 프로그램에서 임포트만 하면 된다.

datetime 모듈은 `date`, `time`, `datetime`, `timethodelta`의 네 가지 주요 클래스를 정의한다. 각 클래스는 날짜와 시간의 다른 측면을 나타내며 고유한 속성과 메서드을 가지고 있다. 각 클래스를 간단히 소개하고 그들이 무엇을 할 수 있는지 살펴보자.

- **`date`** 클래스는 그레고리력으로 날짜(year, month와 day)를 나타낸다. date 클래스를 사용하여 date 객체를 만들고, 날짜를 비교하고, 현재 날짜를 가져오고, 날짜 객체에서 year, month와 day를 추출할 수 있다.
- **`time`** 클래스는 날짜나 시간대에 관계없이 시간(hour, minute, second와 microsecond)을 나타낸다. time 클래스를 사용하여 시간 객체를 만들고 시간을 비교하며 현재 시간을 얻으며 time 객체에서 hour, minute, second와 microsecond를 추출할 수 있다.
- **`datetime`** 클래스는 `date`와 `time` 클래스를 날짜와 시간(year, month, day, hour, minute, second, and microsecond)을 나타내는 단일 객체로 결합한다. datetime 클래스를 사용하여 `datetime` 객체를 생성하고, datwtime을 비교하고, 현재 datetime을 가져와 `datetime` 객체에서 date 및 time 성분을 추출할 수 있다.
- **`timedelta`** 클래스는 두 개의 `date`, `time` 또는 `datetime` 객체 간의 지속시간 또는 차이를 나타낸다. `timedelta` 클래스를 사용하여 `timedelta` 객체를 생성하고, datetime 및 timedelta로 산술 연산을 수행하며, 이벤트 간 경과 시간을 측정할 수 있다.

datetime 모듈은 이 네 가지 클래스 외에도 `MINYEAR`, `MAXYEAR`, `timezone`, `dateutil` 등과 같은 유용한 상수와 함수를 제공한다. 이에 대해서는 이 포스팅의 후반부에서 좀 더 자세히 살펴볼 것이다.

이제 datetime 모듈이 무엇이고 무엇을 할 수 있는지에 대하여 일반적인 이해를 하였으니, 이를 실제로 사용하는 방법을 살펴보자.

## <a name="sec_03"></a> datetime 객체를 만드는 방법
`datetime` 객체를 만들려면 `date`, `time` 및 `datetime` 클래스의 생성자를 사용할 수 있다. 생성자는 클래스에 따라 다른 인수를 사용하지만 모두 `class_name(argument1, argument2, …)` 형식을 따른다. 예를 들어 `date` 객체를 생성하려면 `date(year, month, day)` 생성자를 사용하고 여기서 `year`, `month`, `day`는 date 성분을 나타내는 정수이다. 마찬가지로 `time` 객체를 생성하려면 `time(hour minute, second, microsecond)` 생성자를 사용하고 여기서 `hour`, `minute`, `second`, `microsecond`는 시간 성분을 나타내는 정수이다. `datetime` 객체를 생성하려면 `datetime(year, month, day, hour, minute, second, microsecond)` 생성자를 사용하고 여기서 인수는 date와 time 생성자와 동일한 인수를 사용한다.

생성자를 사용하여 `datetime` 객체를 만드는 방법의 예를 보자. 우리는 객체와 그 유형을 표시하기 위해 `print()` 함수를 사용한다.

```python
# Import the datetime module
import datetime
# Create a date object for January 1, 2020
d1 = datetime.date(2020, 1, 1)
print(d1)
print(type(d1))
# Create a time object for 12:00:00
t1 = datetime.time(12, 0, 0)
print(t1)
print(type(t1))
# Create a datetime object for January 1, 2020 at 12:00:00
dt1 = datetime.datetime(2020, 1, 1, 12, 0, 0)
print(dt1)
print(type(dt1))
```

위의 코드 출력은 아래와 같다.

```
2020-01-01
12:00:00
2020-01-01 12:00:00
```

보다시피 생성자는 해당 클래스의 객체를 반환하고 객체는 기본 형식으로 디스플레이된다. 생성자에 대한 인수는 date와 time 구성 요소에 대해 유효한 값이어야 하며 그렇지 않으면 오류가 발생한다. 예를 들어 `13`과 같이 잘못된 `month` 값을 가진 `date` 객체를 생성하려고 하면 `ValueError`를 얻는다.

`datetime` 객체를 만드는 또 다른 방법은 `datetime` 클래스의 `now()`와 `today()` 메서드를 사용하는 것이다. 이 메서드는 현재 날짜와 시간을 시간대 정보가 있든 없든 `datetime` 객체로 반환한다. `now()` 메서드는 시간대를 지정하는 선택적 인수를 사용하는 반면 `today()` 메서드는 시간대가 없는 로컬 날짜와 시간을 반환한다. 시간대에 대해서는 이 포스팅의 후반부에서 더 자세히 논의할 것이다. 지금은 `now()`와 `today()` 메서드를 사용하는 방법의 예를 살펴보자.

```python
# Import the datetime module
import datetime
# Get the current date and time as a datetime object
dt2 = datetime.datetime.now()
print(dt2)
print(type(dt2))
# Get the current date and time as a datetime object without timezone
dt3 = datetime.datetime.today()
print(dt3)
print(type(dt3))
```

위의 코드 출력은 아래와 같다.

```
2024-03-13 12:09:30.469598
<class 'datetime.datetime'>
2024-03-13 12:09:30.469627
<class 'datetime.datetime'>
```

보다시피 `now()`와 `today()` 메서드는 year, month, day, hour, minute, second와 microsecond 성분과 함께 동일한 `datetime` 객체를 반환한다. 유일한 차이점은 `now()` 메서드는 시간대 정보도 포함할 수 있다는 것인데, 이는 나중에 보게 될 것이다.

지금까지 생성자와 `now()`, `today()` 메서드를 사용하여 `datetime` 객체를 만드는 방법을 살펴보았다. 그러나 strings, timestamps 그리고 다른 datetime 객체 같은 다른 입력으로부터 `datetime` 객체를 만드는 다른 방법들이 있다. 다음 절에서 이 메서드들을 살펴볼 것이다.

## <a name="sec_04"></a> datetime 객체 조작
일단 dattime 객체를 만든 다음에는 산술 연산과 메서드를 사용하여 다양한 방식으로 객체를 조작할 수 있다. datetime 객체를 덧셈, 뺄셈, 비교, 수정하여 다양한 작업과 계산을 수행할 수 있다. 이 절에서는 다음과 같은 기술을 사용하여 datetime 객체를 조작하는 방법을 배울 것이다.

- datetime과 timedelta 객체의 덧셈과 뺄셈
- 관계 연산자를 사용하여 datetime 객체 비교
- 대체와 결합 메서드를 사용하여 datetime 객체 수정
- 속성과 메서드를 사용하여 datetime 객체에서 date와 time 구성 요소 추출

첫 번째 기술인 datetime과 timedelta 객체를 더하고 빼는 것부터 시작하겠다.

#### 덧하기와 빼기

- **`datetime`**과 **`timedelta`**: `datetime` 객체에 `timedelta` 객체를 더하거나 빼서 `datetime` 객체를 앞이나 뒤로 이동시킬 수 있다.

```python
from datetime import datetime, timedelta

# Create a datetime object
today = datetime.now()
# Add one day
tomorrow = today + timedelta(days=1)
# Subtract two weeks
two_weeks_ago = today - timedelta(weeks=2)
print("Today:", today)
print("Tomorrow:", tomorrow)
print("Two weeks ago:", two_weeks_ago)
```

- **datetime과 정수**: day, week 또는 year로 datetime 객체의 특정 구성 요소에 직접 정수를 추가하거나 뺄 수 있다.

```python
# Add 5 days to the day component
five_days_later = today + timedelta(days=5)

# Subtract 3 years from the year component
three_years_ago = today - timedelta(years=3)

print("Five days later:", five_days_later)
print("Three years ago:", three_years_ago)
```

#### 비교 
`==`, `!=`, `<`, `>` 등의 관계 연산자를 사용하여 두 개의 datetime 객체를 비교할 수 있다. 이는 주어진 날짜가 특정 기간에 속하는지 확인하는 것과 같은 작업에 유용하다.

```python
# Check if today is before tomorrow
is_before = today < tomorrow

# Check if two_weeks_ago is not equal to today
not_equal = two_weeks_ago != today

print("Today is before tomorrow:", is_before)
print("Two weeks ago is not equal to today:", not_equal)
```

#### 변경

- **`replace()` 메서드**: 이 메서드를 사용하면 다른 구성 요소를 변경하지 않고 새 datetime 객체를 만들 수 있다.

```python
# Replace the hour and minute of today with 10 and 30 respectively
modified_today = today.replace(hour=10, minute=30)

print("Original:", today)
print("Modified:", modified_today)
```

- **`combine()` 메서드(depreced)**: 새 코드에는 권장되지 않지만 이 메서드는 date와 time 별도의 객체을 하나의 datetime 객체로 결합한다.

```python
# Combine today's date with a specific time (12 PM)
from datetime import time

specific_time = time(hour=12)
combined_datetime = today.combine(specific_time)

print("Combined datetime:", combined_datetime)
```

#### 구성요소 추출
여러 속성과 메서드는 year, month, day, hour, minute 등 datetime 객체에서 특정 구성 요소를 추출하는 데 도움이 된다.

```python
# Get the year, month, and day
year = today.year
month = today.month
day = today.day

# Get the hour and minute
hour = today.hour
minute = today.minute

print("Year:", year)
print("Month:", month)
print("Day:", day)
print("Hour:", hour)
print("Minute:", minute)
```

## <a name="sec_05"></a> datetime 객체의 형식 지정과 구문 분석
날짜와 시간을 작업할 때 가장 일반적인 작업 중 하나는 문자열을 이용하여 datetime 객체를 포맷하고 파싱하는 것이다. 포맷팅은 datetime 객체를 문자열 표현으로 변환하는 것을 의미하며, 파싱은 문자열 표현을 datetime 객체로 변환하는 것을 의미한다. 포맷팅과 파싱은 다양한 방식으로 날짜와 시간 데이터를 표시, 저장 및 처리하는 데 유용하다.

datetime 객체를 포맷하고 구문 분석하려면 datetime 클래스의 `strftime()`과 `strptime()` 메서드를 사용한다. `strftime()` 메서드는 datetime 객체와 문자열 형식(format string)을 인수로 사용하고 형식의 문자열(formatted string)을 반환한다. `strptime()` 메서드는 형식의 문자열과 문자열 형식을 인수로 사용하고 datetime 객체를 반환한다. 문자열 형식은 다양한 자리 표시자 또는 지시자를 사용하여 날짜와 시간 구성 요소가 문자열에 어떻게 표시되는지 지정한다. 예를 들어, 지시자 `%Y`는 4자리 숫자로 연도를 나타내고, 지시자 `%m`은 두 자리 숫자로 월을 나타낸다.

`strftime()`과 `strptime()` 메서드를 사용하는 예를 보자. 우리는 결과와 그 유형을 표시하기 위해 `print()` 함수를 사용하겠다.

```python
# Import the datetime module
import datetime
# Create a datetime object for November 23, 2023 at 11:35:42
dt = datetime.datetime(2023, 11, 23, 11, 35, 42)
# Format the datetime object as a string using the default format
s1 = dt.strftime("%c")
print(s1)
print(type(s1))
# Format the datetime object as a string using a custom format
s2 = dt.strftime("%A, %B %d, %Y at %I:%M:%S %p")
print(s2)
print(type(s2))
# Parse the string as a datetime object using the same format
dt2 = datetime.datetime.strptime(s2, "%A, %B %d, %Y at %I:%M:%S %p")
print(dt2)
print(type(dt2))
```

위의 코드 출력은 아래와 같다.

```
Thu Nov 23 11:35:42 2023
<class 'str'>
Thursday, November 23, 2023 at 11:35:42 AM
<class 'str'>
2023-11-23 11:35:42
<class 'datetime.datetime'>
```

보다시피 `strftime()` 메서드는 문자열 형식을 템플릿으로 사용하여 datetime 객체의 문자열 표현을 반환한다. `strptime()` 메서드는 문자열 형식을 가이드로 사용하여 문자열 표현과 일치하는 datetime 객체를 반환한다. 문자열 향식은 문자열 표현과 정확히 일치해야 한다. 그렇지 않으면 **`ValueError`**를 받게 된다.

문자열을 사용하여 datetime 객체를 포맷하고 구문 분석하는 데 사용할 수 있는 많은 지시문이 있다. [Python 문서]()에서 명령어의 전체 목록과 그 의미를 찾을 수 있다. 가장 일반적인 지시문은 다음과 같다.

![](./1_1BuP3gPUJ3RYGD16nAnjUg.webp)

이러한 지시문을 사용하여 필요와 기본 설정에 따라 datetime 객체를 다양한 방식으로 포맷하고 구문 분석할 수 있다. 
그러나 `strftime()`과 `strptime()` 메서드의 출력은 시스템의 로케일 설정에 따라 달라질 수 있다. 로케일은 사용자의 언어, 국가와 문화 관습을 정의하는 매개 변수의 집합이다. 
예를 들어, 미국의 날짜 형식은 일반적으로 MM/DD/YYYY인 반면 영국은 일반적으로 DD/MM/YYYY이다. 혼란과 불일치를 방지하기 위해 ISO 8601 형식과 같이 로케일과 독립적인 표준 형식을 사용할 수 있다. ISO 8601 형식은 YYYY-MM-DDTHH:MM:SS이며, 
여기서 T는 날짜와 시간 구성 요소 사이의 구분 기호입이다. 예를 들어, 2023년 11월 23일 11시 35분 42초의 ISO 8601 표현은 2023–11–23T11:35:42이다.

ISO 8601 형식을 사용하여 datetime 객체를 포맷하고 구문 분석하려면 datetime 클래스의 `isoformat()`과 `fromisoformat()` 메서드를 사용할 수 있다. `isoformat()` 메서드는 `datetime` 객체를 사용하여 ISO 8601 
형식의 문자열 표현을 반환한다. `fromisoformat()` 메서드는 ISO 8601 형식의 문자열 표현으로부터 datetime 개체를 반환한다. 이러한 메서드를 사용하는 방법의 예를 살펴보겠다.

```python
# Import the datetime module
import datetime
# Create a datetime object for November 23, 2023 at 11:35:42
dt = datetime.datetime(2023, 11, 23, 11, 35, 42)
# Format the datetime object as a string using the ISO 8601 format
s = dt.isoformat()
print(s)
print(type(s))
# Parse the string as a datetime object using the ISO 8601 format
dt3 = datetime.datetime.fromisoformat(s)
print(dt3)
print(type(dt3))
```

위의 코드 출력은 아래와 같다.

```
2023-11-23T11:35:42
<class 'str'>
2023-11-23 11:35:42
<class 'datetime.datetime'>
```

보다시피 `isoformat()`과 `fromisoformat()` 메서드는 동일한 datetime 객체를 반환하지만 문자열 표현은 다르다. ISO 8601 형식은 날짜와 시간을 표현하는 표준적이고 모호하지 않은 방법이며 다양한 어플리케이션과 
시스템에서 널리 사용되고 있다.

이 절에서는 문자열을 사용하여 datetime 개체를 포맷하고 구문 분석하는 방법을 배웠다. 다양한 지시어를 사용하여 `strftime()`과 `strptime()` 메서드를 사용하는 방법과 ISO 8601 형식을 사용하여 
`isoformat()`과 `fromisoformat()` 메서드를 사용하는 방법을 살펴보았다. 

## <a name="sec_06"></a> 시간대와 일광 절약 시간 처리
날짜와 시간을 다루는 데 있어 가장 어려운 측면 중 하나는 시간대와 일광 절약 시간을 다루는 것이다. 시간대는 동일한 표준 시간을 갖는 지구의 지역이며, 일광 절약 시간은 여름 달 동안 낮을 더 잘 사용하기 위해 
시계를 한 시간 앞당기는 관행이다. 특히 다른 출처와 위치의 데이터를 다룰 때, 시간대와 일광 절약 시간은 날짜와 시간 계산과 비교의 정확성과 일관성에 영향을 미칠 수 있다.

시간대와 일광절약 시간을 처리하기 위해서는 시간대 정보를 종합적으로 수집하는 외부 라이브러리인 `pyz` 모듈을 사용하면 된다. `pyz` 모듈은 표준 라이브러리에 속하지 않으므로 pip 명령이나 다른 패키지 관리자를 사용하여 설치해야 한다. 
`pyz` 모듈에 대한 자세한 내용과 설치 방법은 [공식 문서]()에서 확인할 수 있다.

`pytz` 모듈은 시간대 객체를 나타내는 `timezone`이라는 클래스를 정의한다. `timezone` 클래스를 사용하여 'UTC', '미국/동부' 또는 '유럽/런던'과 같은 다양한 시간대 이름으로 timezone 객체를 만들 수 있다. 시간대 이름의 전체 목록은 
[pytz reference]()에서 찾을 수 있다. 시간대 객체를 만든 다음에는 `localize()`와 `astimezone()` 메서드를 사용하여 특정 시간대에 datetime 객체를 부착하거나 변환하는 데 사용할 수 있다. 
`localize()` 메서드는 단순한 datetime 객체(시간대 정보가 전혀 없는 datetime 객체)를 취하여 로컬라이즈된 datetime 객체(특정 시간대 정보가 있는 datetime 객체)를 반환한다. `astimezone()` 메서드는 
로컬라이즈된 datetime 객체를 취하고 다른 시간대로 변환된 datetime 객체를 반환한다.

`pytz` 모듈과 `timezone` 클래스를 사용하는 방법의 예를 보자. 우리는 결과와 그 타입을 표시하기 위해 `print()` 함수를 사용하였다.

```python
# Import the datetime and pytz modules
import datetime
import pytz
# Create a naive datetime object for November 23, 2023 at 11:35:42
dt = datetime.datetime(2023, 11, 23, 11, 35, 42)
print(dt)
print(type(dt))
# Create a time zone object for UTC
tz_utc = pytz.timezone('UTC')
print(tz_utc)
print(type(tz_utc))
# Localize the datetime object to UTC
dt_utc = tz_utc.localize(dt)
print(dt_utc)
print(type(dt_utc))
# Create a time zone object for US/Eastern
tz_us_eastern = pytz.timezone('US/Eastern')
print(tz_us_eastern)
print(type(tz_us_eastern))
# Convert the datetime object to US/Eastern
dt_us_eastern = dt_utc.astimezone(tz_us_eastern)
print(dt_us_eastern)
print(type(dt_us_eastern))
```

위의 코드 출력은 아래와 같다.

```
2023-11-23 11:35:42
<class 'datetime.datetime'>
UTC
<class 'pytz.UTC'>
2023-11-23 11:35:42+00:00
<class 'datetime.datetime'>
US/Eastern
<class 'pytz.tzfile.US/Eastern'>
2023-11-23 06:35:42-05:00
<class 'datetime.datetime'>
```

보다시피, `pytz` 모듈과 `timezone` 클래스를 사용하면 서로 다른 시간대를 갖는 `timezone` 객체를 생성, 로컬라이제이션 및 변환할 수 있다. 시간대 정보는 +00:00 또는 -05:00과 같이 UTC와의 오프셋으로 표시된다. 
오프셋은 시간대의 일광 절약 시간 규칙에 따라 달라질 수 있다. `pytz` 모듈은 일광 절약 시간 전환을 자동으로 처리하므로 걱정할 필요가 없다.

이 절에서는 `pytz` 모듈과 `timezone` 클래스를 이용하여 시간대와 일광절약시간을 다루는 방법을 배웠다. 시간대가 다른 datetime 객체를 만들고, 위치를 파악하고, 변환하는 방법, 일광절약시간 전환을 다루는 방법 등을 살펴보았다. 
시간대와 일광 절약시간을 다루는 것은 까다로울 수 있지만, `pytz` 모듈을 사용하면 더 쉽고 신뢰할 수 있다.

## <a name="sec_07"></a> `timedelta`와 `dateutil` 모듈
앞 절에서 datetime과 pytz 모듈을 사용하여 datetime 객체를 생성하고, 조작하고, 포맷하고, 파싱하는 방법을 배웠다. 그러나 timeelta와 dateutil 모듈처럼 날짜와 시간에 대해 더 복잡한 계산과 연산을 수행하는 데 도움이 될 수 있는 모듈도 있다. 이 절에서는 이러한 모듈을 사용하여 다음 작업을 수행하는 방법을 살펴볼 것이다.

- timedelta 객체를 생성하고 사용하여 datetime 객체 간의 기간과 차이 표시한다.
- dateutil 모듈을 사용하여 다양한 형식과 언어로 datetime 문자열을 구문 분석한다.
- dateutil 모듈을 사용하여 캘린더 계산과 반복 이벤트를 처리한다.
- dateutil 모듈을 사용하여 일광 절약 시간 전환으로 인해 존재하지 않는 애매 모호한 시간을 처리한다.

`timedelta`: 이 내장 모듈은 시간의 지속 시간을 나타내는 객체를 제공함으로써 `datetime`을 보완한다.

`dateutil`: 이 외부 라이브러리는 특정 날짜 형식을 구문 분석하고 캘린더 계산을 처리하며 일광 절약 시간과 같은 복잡성을 관리하기 위한 고급 기능을 제공한다.

첫 번째 주제인 timedelta 객체를 만들고 사용하는 것부터 시작하겠다.

### `timedelta` 객체 생성과 사용
`timedelta` 객체는 특정 달력 날짜와는 무관한 시간을 나타낸다. 다양한 키워드 인수를 사용하여 `tiemdelta` 객체를 만들 수 있다.

```python
from datetime import timedelta

# Create a timedelta object representing 3 days, 2 hours, and 15 minutes
duration = timedelta(days=3, hours=2, minutes=15)
# Access individual components
days = duration.days
hours = duration.seconds // 3600  # Integer division to get hours
minutes = (duration.seconds % 3600) // 60
print("Duration:", duration)
print("Days:", days)
print("Hours:", hours)
print("Minutes:", minutes)
```

#### `timedelta` 객체 사용

- **`datetime` 객체에 덧셈과 뺄셈**: `timedelta` 객체를 datetime 객체에 더하거나 빼어 시간을 앞으로 이동하거나 뒤로 이동한다.

```python
from datetime import datetime

today = datetime.now()
# Add the duration to today
later = today + duration
# Subtract the duration from today
earlier = today - duration
print("Today:", today)
print("Later:", later)
print("Earlier:", earlier)
```

- **`timedelta` 객체로 계산하기**: `timedelta` 객체를 덧셈, 뺄셈, 곱셈 또는 나눗셈을 하여 다양한 시간 기반 계산을 수행할 수 있다.

```python
# Double the duration
double_duration = duration * 2

# Calculate the difference between two datetime objects
time_elapsed = today - later
print("Double duration:", double_duration)
print("Time elapsed:", time_elapsed)
```

### `dateutil` 모듈 사용

`dateutil` 라이브러리는 핵심 `datetime` 모듈 이상의 풍부한 기능을 제공한다. 다음은 주요 하이라이트이다.

- **다양한 형식으로 날짜 파싱**

```python
from dateutil import parser

# Parse a date string in a specific format (e.g., YYYY-MM-DD)
date_object = parser.parse("2024-02-29")
print("Parsed date:", date_easter)
```

- **달력 계산 수행**

```python
from dateutil.relativedelta import relativedelta

# Get the date 6 months from today
six_months_later = today + relativedelta(months=6)
# Check if a specific date is a weekday
is_weekday = six_months_later.weekday() < 5  # Weekday is 0-4, Saturday is 5, Sunday is 6
print("Six months later:", six_months_later)
print("Is weekday:", is_weekday)
```

- **반복 이벤트 처리**

```python
from dateutil.rrule import rrule

# Define a rule for every second Wednesday of the month
rule = rrule.rrule(freq=rrule.MONTHLY, interval=1, byweekday=rrule.WEDNESDAY, wkday=2)
# Generate a list of occurrences for the next year
occurrences = list(rule.between(today, today + timedelta(days=365)))
print("Next year's occurrences:", occurrences)
```

- **일광 절약 시간(DST) 처리**

```python
from dateutil import tz

# Get the local timezone
local_timezone = tz.localzone
# Create a datetime object with DST awareness
dst_aware_datetime = local_timezone.localize(datetime(2023, 11, 5, 1))  # Consider a date with DST transition
# Check if DST is in effect for the datetime
is_dst = dst_aware_datetime.dst()
print("Is DST in effect:", is_dst)
```

다음은 `timedelta`와 `dateutil`이 제공하는 기능 중 몇 가지 예에 불과하다. 포괄적인 [문서]()에서 보다 자세한 정보를 얻을 수 있다.

## <a name="summary"></a> 요약
이 포스팅에서는 Python에서 datetime 모듈을 사용하여 날짜와 시간 작업dmf 하는 방법을 배웠다. 다음과 같은 방법을 살펴보았다.

- 생성자, 메서드 및 함수를 사용하여 다양한 입력에서 datetime 객체 생성
- 산술 연산과 메서드를 사용하여 datetime 객체 조작
- 문자열과 지시어를 사용하여 datetime 객체 형식 지정과 구문 분석
- pytz 모듈과 timezone 클래스를 사용하여 시간대와 일광 절약 시간 처리
- timedelta와 dateutil 모듈을 사용하여 날짜와 시간에 따라 더 복잡한 계산과 연산 수행

이 포스팅을 따라 Python에서 날짜와 시간을 다루는 방법과 datetime 모듈을 효율적으로 사용하는 방법에 대해 잘 이해할 수 있었다. 
또한 날짜와 시간 데이터를 다룰 때 일반적인 함정과 오류를 피하기 위한 유용한 팁과 요령을 배웠다.
