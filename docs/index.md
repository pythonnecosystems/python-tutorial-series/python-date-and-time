# Python Date와 Time: Datetime 사용법 <sup>[1](#footnote_1)</sup>

<font size="3">Python에서 `datetime` 모듈을 임포트하여 날짜와 시간을 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./date-and-time.md#intro)
1. [`datetime` 모듈이란?](./date-and-time.md#sec_02)
1. [datetime 객체를 만드는 방법](./date-and-time.md#sec_03)
1. [datetime 객체 조작](./date-and-time.md#sec_04)
1. [datetime 객체의 형식 지정과 구문 분석](./date-and-time.md#sec_05)
1. [시간대와 일광 절약 시간 처리](./date-and-time.md#sec_06)
1. [`timedelta`와 `dateutil` 모듈](./date-and-time.md#sec_07)
1. [요약](./date-and-time.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 26 — Python Date and Time: Working with Datetime](https://python.plainenglish.io/python-tutorial-26-python-date-and-time-working-with-datetime-f96413a42281?sk=13d88d343b9da1d631d505e0aec91c63)를 편역하였다.
